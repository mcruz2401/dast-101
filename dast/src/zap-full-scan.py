#!/usr/bin/env python

# This script wraps zap-full-scan.py, which is coming with OWASP ZAP, for the purpose of supporting
# the syntax of GitLab DAST jobs as defined at https://docs.gitlab.com/ee/ci/examples/dast.html

import os
import argparse
import logging
import sys
import zap_full_scan

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
# Hide "Starting new HTTP connection" messages
logging.getLogger("requests").setLevel(logging.DEBUG)

# Supported parameters for authentication
auth_params = [
    '--auth-first-page',
    '--auth-url',
    '--auth-username',
    '--auth-password',
    '--auth-username-field',
    '--auth-password-field',
    '--auth-first-submit-field',
    '--auth-submit-field',
    '--auth-exclude-urls'
]

# usage() should be called when zap_full_scan.usage() is called. Do this with monkey patching.
original_usage = zap_full_scan.usage
def usage():
    original_usage()
    print('')
    print ('Authentication:')
    print ('    --auth-url                 login form URL')
    print ('    --auth-username            username')
    print ('    --auth-password            password')
    print ('    --auth-username-field      name of username input field')
    print ('    --auth-password-field      name of password input field')
    print ('    --auth-submit-field        name or value of submit input')
    print ('    --auth-first-page          enable two-page authentication')
    print ('    --auth-first-submit-field  name or value of submit input of first page')
    print ('    --auth-exclude-urls        comma separated list of URLs to exclude, supply all URLs causing logout')
    print ('')
    print ('For more details on authentication parameters see https://docs.gitlab.com/ee/ci/examples/dast.html')

def main(argv):
    try:
        parser = argparse.ArgumentParser()
        for p in auth_params:
            parser.add_argument(p)

        args, unknown = parser.parse_known_args(argv)

        for opt, arg in vars(args).iteritems():
            if arg is not None:
                os.environ[opt.upper()] = arg

        
        zap_full_scan.usage = usage
        zap_full_scan.main(unknown)
    except Exception as e:
    	raise
    else:
    	pass
    finally:
    	pass

if __name__ == "__main__":
    main(sys.argv[1:])
