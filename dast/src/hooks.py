import zap_webdriver
import logging

def zap_access_target(zap, target):
    webdriver = zap_webdriver.ZapWebdriver()
    webdriver.load_from_environment_vars()

    try:
        if webdriver.is_authentication_required():
            webdriver.setup_zap_context(zap, target)
            webdriver.setup_webdriver(zap, target)
            webdriver.login(zap, target)
    except Exception as e:
    	logging.error('Authentication Error')
    	logging.exception(e)
    finally:
        webdriver.cleanup()

def cli_opts(opts):
	logging.debug('CLI opts')
	logging.debug(opts)
