# GitLab DAST

Este proyecto está orientado a mostrar conceptos básicos de integración y entrega contínua de software y análisis dinámico de aplicaciones con GitLab.

## Comenzando 🚀

_Esta introducción permitirá obtener una breve explicación sobre el funcionamiento e implementación de los proyectos de CI/CD._

Partiendo de la necesidad de automatizar el proceso de compilación y despliegue de una aplicación, podemos crear un _pipeline_, que empuje nuestro código en 3 fases:
_build_, _test_ y _deploy_. Un pipeline es un grupo de pasos que son agrupados bajo características similares. Con estas fases o etapas nuestro pipeline es definido en 3 tipos:

* El pipeline del proyecto (project pipeline).
* El Pipeline de integración contínua.
* El pipeline de entrega.

El pipeline del proyecto instala dependencias, corre los linters y cualquier script que tenga que ver con código. El pipeline de integración continua corre pruebas automatizadas y construye versiones distribuidas del código. Finalmente, el pipeline de entrega, entrega el código a un ambiente de un proveedor designado.

```
A.) Build
     i. Install NPM Dependencies
     ii. Run ES-Linter
     iii. Run Code-Minifier
B.) Test
     i. Run unit, functional and end-to-end test
     ii. Run pkg to compile Node.js application
C.) Deploy
     i. Production
        i. Launch EC2 instance on AWS
     ii. Staging
        i. Launch on local development server
```

En esta jerarquía, los tres componentes son considerados tres diferentes pipelines. Las partes importantes — build, test y deploy — son etapas (_stages_) y cada parte debajo de estas secciones son trabajos (_jobs_).

### Usando GitLab CI/CD 🔧

Para usar GitLab CI/CD, creamos un archivo con nombre `.gitlab-ci.yml` en la raíz del proyecto de nuestro repositorio y agregamos el siguiente código yaml:

```yaml
image: node:10.5.0

stages:
  - build
  - test
  - deploy

before_script:
  - npm install
```

GitLab utiliza [runners](https://docs.gitlab.com/runner/) para ejecutar los pipelines. Podemos definir el sistema operativo y librerías predefinidas en las cuales queremos basar nuestros runners usando la directiva _image_. La directiva _stages_ permite que pre-definamos una etapa para la un proceso de configuración completo. Los jobs serán ejecutados basados en el orden listado en la directiva [stages](https://docs.gitlab.com/ee/ci/yaml/#stages). La directiva _before-script_ es usada para correr un comando de manera previa a la ejecución de jobs. La directiva _script_ es el caparazón del script que se ejecuta dentro del runner. Se puede asignar un job a una etapa específica usando la directiva stage.

```yaml
build-min-code:
  stage: build
  script:
    - npm install
    - npm run minifier
```

Es posible crear un job de prueba _run-unit-test_ ejecutando el script npm para correr una prueba `npm test`.

```yaml
run-unit-test:
  stage: test
  script:
    - npm run test
```

Finalmente, es necesario agregar un job destinado a la etapa de entrega (`deploy-production`, `deploy-staging`). En esta instancia nos encontramos con dos trabajos diferentes para el _deployment_ (staging y production) siguiendo las [buenas prácticas de despliegue](https://docs.gitlab.com/ee/ci/environments.html). Estos trabajos reflejarán el mismo layout que el trabajo previo pero con una modificación. Utilizando la directiva _only_ es posible definir en que _branch_ específico se ejecuta un job determinado. La directiva only define las etiquetas para los trabajos que deberán correr en el [branch](https://docs.gitlab.com/ee/user/project/repository/branches/) especificado.

```yaml
deploy-staging:
 stage: deploy
 script:
   - npm run deploy-stage
 only:
   - develop

deploy-production:
 stage: deploy
 script:
   - npm run deploy-prod
 only:
   - master
```

La estructura completa del archivo `.gitlab-ci.yaml`, es la siguiente:

```yaml
image: node:10.5.0

stages:
  - build
  - test
  - deploy

before_script:
  - npm install

build-min-code:
  stage: build
  script:
    - npm install
    - npm run minifier

run-unit-test:
  stage: test
  script:
    - npm run test

deploy-staging:
  stage: deploy
  script:
    - npm run deploy-stage
  only:
    - develop

deploy-production:
  stage: deploy
  script:
    - npm run deploy-prod
  only:
    - master
```

### Integrando Seguridad 🔧

Es posible integrar controles de seguridad al proceso automático de construcción y despliegue de software. El análisis dinámico de aplicaciones o DAST (Dynamic Application Security Testing) es un test de seguridad para aplicaciones de tipo "grey box", cuya finalidad consiste en testear el _runtime_ de una aplicación para asegurar la detección temprana de vulnerabilidades.

GitLab, en su versión enterprise, provee una versión adaptada del framework [ZAP](https://github.com/zaproxy/zaproxy/wiki/Docker) de OWASP. Sin embargo, es posible utilizar un _fork_ del [proyecto](https://gitlab.com/gitlab-org/security-products/dast) como en este repositorio, generando los cambios necesarios para adaptar los controles a cada caso de uso, e implementarlo en proyectos creados en las versiones open source (Community Edition) de GitLab.

En el ejemplo actual se corre un análisis activo de una aplicación vulnerable dentro del mismo _runner_. Del mismo modo, es posble llevar a cabo un análisis profundo de plataformas que requieren autorización, creando un job con una esquema similar al siguiente:

```yaml
dast:
  before_script:
    - apk add --update --no-cache git
  script:
    - docker build -f dast/Dockerfile -t dast dast/
    - docker run --net=dast -v $(pwd)/wrk:/zap/wrk/:rw -t dast /analyze -t http://192.168.0.1:1234
      --auth-url "http://192.168.0.1:1234/login.html" \
      --auth-username '<user>' \
      --auth-password '<pass>' \
      --auth-username-field "login" \
      --auth-password-field "pass" \
      --auth-submit-field "login_btn" \
      --auth-exclude-urls "http://192.168.0.1:1234/logout.html" || true
  artifacts:
    paths:
      - wrk/report.html
    expire_in: 1 week
```

Los [reportes](reports/ZAP%20Scanning%20Report.html) de vulnerabilidades se conservan como [artefactos](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html) facilitando el acceso y conservación de los archivos, y su posterior utilización en un plan de [Gestión de Vulnerabilidades](https://dsomm.timo-pagel.de/).